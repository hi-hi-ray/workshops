# Aula Git Unicorn
Esse repositório é referente ao workshop github 101, realizado no mana pirata dia 01/07/2017 :octocat:

### Deixando sua documentação bonita.

* [Formantando o seu Markdown](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf)
* [Lista de Emoticons](https://www.webpagefx.com/tools/emoji-cheat-sheet/)

#### Emoticons e seus significados em um commit

- :art: Ao melhorar o formato / estrutura do código
- :rocket: Ao melhorar o desempenho
- :pencil2: Ao escrever documentos
- :bulb: Nova ideia
- :construction: Trabalho em andamento
- :heavy_plus_sign: Ao adicionar recurso
- :heavy_minus_sign: Ao remover o recurso
- :speaker: Ao adicionar log
- :mute: Ao reduzir o log
- :bug: Ao corrigir um bug
- :white_check_mark: Ao adicionar testes
- :lock: Quando se lida com segurança
- :arrow_up: Ao atualizar dependências
- :arrow_down: Ao desatualizar dependências

##### Documentações importantes pra um repositório

Nome | Modelo
------------ | -------------
Readme | [Documentação básica e principal do seu projeto](https://gist.github.com/hi-hi-ray/4b54aa4996604cc76c05304c6fa7cae1)
Readme com Docker | [Documentação básica e principal do seu projeto com docker](https://gist.github.com/hi-hi-ray/6ca085c96c6bd5e96c6b1609218d24ce)
Contributing | [Como uma pessoa pode contribuir com seu repositório](https://gist.github.com/hi-hi-ray/a868081e2a63ee47fafa015353d05ae3)
License | [Copyrights do Seu Repositório](https://gist.github.com/hi-hi-ray/b726af156bdfb463dd10025ae76ea4c1)
Changelog | [Lista de Mudanças, sejam features ou fixes da sua aplicação](http://keepachangelog.com/pt-BR/0.3.0/)

###### Sabendo se seu repósitorio * pode ser open source

[Requisitos para projetos open source](https://gist.github.com/hi-hi-ray/b5db2e2dff3d14175403078585e609dd)


@[Endereço da Página](https://hi-hi-ray.github.io/aula-git-unicorn/)
